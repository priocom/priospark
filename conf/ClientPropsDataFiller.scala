import java.util.concurrent.CopyOnWriteArrayList

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.hbase.{HColumnDescriptor, HTableDescriptor, TableName, HBaseConfiguration}
import org.apache.hadoop.hbase.client.{Put, ConnectionFactory}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Try

/**
 * Created by jcrm on 20.04.16.
 */
case class ClientPropsDataFiller(phone: String, phoneType: Int, isBlacklisted: Boolean) extends Serializable

val table = Bytes.toBytes("CLIENT_PROPS")
val tableCF = Bytes.toBytes("CLIENT_PROPS_CF")

val phoneTypeColumn = Bytes.toBytes("phone_type")
val isBlacklistedColumn = Bytes.toBytes("is_blacklisted")

val conf = HBaseConfiguration.create()
val connection = Try(ConnectionFactory.createConnection(conf)).orElse(throw new RuntimeException()).get
val admin = connection.getAdmin()

val config = ConfigFactory.load()
val interval = config.getInt("interval")
val sourceDir = config.getString("sourseDir")
val	sc	=	new	SparkContext("local[*]",	"CDRConverter",	new	SparkConf().set("spark.scheduler.mode", "FAIR"))
val ssc = new StreamingContext(sc, batchDuration = Seconds(interval))
val sqlContext = new SQLContext(sc)

val cdrs = ssc.textFileStream(sourceDir).map(s => {
  val phone = s.split(",")(3)
  val pattern = phone.lastOption
  val phoneType = if (pattern.isEmpty) 0 else if (pattern.get.toInt % 2 == 0) 1 else 2
  val isBlacklisted = if (pattern.isEmpty || pattern.get.toInt < 5) false else true

  new ClientPropsDataFiller(phone, phoneType, isBlacklisted)
})

if (!admin.isTableAvailable(TableName.valueOf(table))) {
  val tableDescriptor = new HTableDescriptor(TableName.valueOf(table))
  val cf = new HColumnDescriptor(tableCF)
  tableDescriptor.addFamily(cf)

  admin.createTable(tableDescriptor)
}

cdrs.foreachRDD(rdd => {
  if (!rdd.isEmpty()) {
    val time = System.currentTimeMillis()
    val tableDesc = connection.getTable(TableName.valueOf(Bytes.toString(table)))
    val clientProps = new CopyOnWriteArrayList[Put]()
    rdd.foreachPartition(iter => {
      iter.foreach(data => {
        val put = new Put(Bytes.toBytes(data.phone))
        put.addColumn(tableCF, phoneTypeColumn, Bytes.toBytes(data.phoneType))
        put.addColumn(tableCF, isBlacklistedColumn, Bytes.toBytes(data.isBlacklisted))
        clientProps.add(put)
      })
    })
    tableDesc.put(clientProps)
    println(System.currentTimeMillis() - time)
    tableDesc.close()
  }
})

