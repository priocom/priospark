package priospark.util


/**
 * Created by vitalyshpak on 5/2/16.
 */

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
//import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import priospark.api.Event

object Json {
  val mapper = new ObjectMapper() // with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  //def toMap[V](json:String)(implicit m: Manifest[V]) = fromJson[Map[String,V]](json)

 // def toMap (json:String) : Map  = fromJsonToMap(json: String)

  /*
  def fromJson[T](json: String)(implicit m : Manifest[T]): T = {
    //mapper.readValue[T](json)
    //mapper.readValue(json, Class[T])
    //mapper.readValue[T](json, T)

    //mapper.readValue(json, ClassOf[])
  }*/


  def fromJsonToMap(json: String) : Map[String,List[Map[String,String]]] = {
    mapper.readValue(json, classOf[Map[String,List[Map[String,String]]]])
  }

}
