package priospark.service

import java.sql.Timestamp
import java.util
import java.util.Calendar
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.TimeUnit

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp
import org.apache.hadoop.hbase.filter.{SingleColumnValueFilter, FilterList}
import org.apache.ignite.cache.CacheAtomicityMode
import org.apache.ignite.cache.affinity.AffinityFunction
import org.apache.ignite.configuration.{IgniteConfiguration, CacheConfiguration}
import org.apache.ignite.{Ignite, Ignition}
import priospark.api._
import com.typesafe.config.ConfigFactory
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase._
import org.apache.hadoop.hbase.client._
import org.apache.spark.sql.{SaveMode, SQLContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j._
import org.apache.ignite.spark._


import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.util.matching.Regex

/**
 * Created by jcrm on 14.04.16.
 */
object CDRConverter {

  final val subscriberTable = Bytes.toBytes("SUBSCRIBER")
  final val subscriberLastCallTable = Bytes.toBytes("SUBSCRIBER_LAST_CALL")
  final val clientPropsTable = Bytes.toBytes("CLIENT_PROPS")


  final val subscriberTableCF = Bytes.toBytes("subscriber_cf")
  final val subscriberLastCallTableCF = Bytes.toBytes("subscriber_last_call_cf")
  final val clientPropsTableCF = Bytes.toBytes("client_props_cf")

  final val subscriberNameColumn = Bytes.toBytes("name")
  final val subscriberLacColumn = Bytes.toBytes("lac")
  final val subscriberTimeFromColumn = Bytes.toBytes("time_from")
  final val subscriberTimeToColumn = Bytes.toBytes("time_to")
  final val subscriberNotificationTypeColumn = Bytes.toBytes("notification_type")
  final val subscriberNotificationAddressColumn = Bytes.toBytes("notification_address")
  final val subscriberNotificationTextColumn = Bytes.toBytes("notification_text")

  final val subscriberLastCallTimeColumn = Bytes.toBytes("time")
  final val subscriberLastCallIsWorkingTimeColumn = Bytes.toBytes("is_working_time")

  final val clientPropsPhoneTypeColumn = Bytes.toBytes("phone_type")
  final val clientPropsIsBlacklistedColumn = Bytes.toBytes("is_blacklisted")

  val conf = HBaseConfiguration.create()
  val connection = Try(ConnectionFactory.createConnection(conf)).orElse(throw new RuntimeException()).get
  val admin = connection.getAdmin()

  val config = ConfigFactory.load()
  val master = config.getString("masterUrl")
  val interval = config.getInt("interval")
  val sourceDir = config.getString("sourseDir")
  val hbaseMaxVersions = config.getInt("hbaseMaxVersions")
  val cdrParquetDir = config.getString("cdrParquetDir")
  val notificationParquetDir = config.getString("notificationParquetDir")
  val eventParquetDir = config.getString("eventParquetDir")
  val fileActionCommand = ActionType.withName(config.getString("action"))
  val filesCopyDir = new Path(config.getString("filesCopyDir"))
  val cacheFilename = config.getString("cacheFilename")

  def main(args: Array[String]) {
    val	sc	=	new	SparkContext(master,	"CDRConverter",	new	SparkConf().set("spark.scheduler.mode", "FAIR"))
    val ssc = new StreamingContext(sc, batchDuration = Seconds(interval))
    val sqlContext = new SQLContext(sc)
    val cdrs = ssc.textFileStream(sourceDir).map(CDR.parseSensor)

    val ignite = Ignition.start(cacheFilename)

    val cache = ignite.getOrCreateCache[String, LastCallBack]("subscriberCalls")

    val logger = Logger.getLogger(getClass().getName())

    import sqlContext.implicits._
    import scala.collection.JavaConversions._
    import priospark.service.CDRSubscriptionProcess


    if (!admin.isTableAvailable(TableName.valueOf(subscriberTable)))
      createSubscriberTable()
    if (!admin.isTableAvailable(TableName.valueOf(subscriberLastCallTable)))
      createSubscriberLastCallTable()
    if (!admin.isTableAvailable(TableName.valueOf(clientPropsTable)))
      createClientPropsTable()

    val fileRegExp = new Regex("(/.*|\\w*).csv")

    // init subscriptions
    val subs = new CDRSubscriptionProcess
    val subsVals = subs.subscrVals
    logger.info("Init subscribers:"+subs.getSubscriberSize)


    cdrs.foreachRDD(rdd => {
      if (!rdd.isEmpty()) {
        val dataFrames = rdd.toDF()
        dataFrames.write.mode(SaveMode.Append).parquet(cdrParquetDir)

        logger.info("Event processing RDD")

        /* make subscriber last call partition */
        rdd.keyBy(cdr => cdr.MSISDN ).reduceByKey((cdr1, cdr2) => {
          if (cdr1.OPENING_TIME.compareTo(cdr2.OPENING_TIME) > 0)
            cdr1
          else cdr2
        }).flatMap(msisdn_cdr => {
          val events = new util.ArrayList[Event]()
          val lastCallTableDef = connection.getTable(TableName.valueOf(Bytes.toString(subscriberLastCallTable)))
          val subscriberLastCalls = new CopyOnWriteArrayList[Put]()

          val cdr = msisdn_cdr._2

          val lastCallIgnite = cache.get(cdr.MSISDN)

          val get = new Get(Bytes.toBytes(cdr.MSISDN))
          get.addColumn(subscriberLastCallTableCF, subscriberLastCallTimeColumn)

          if (lastCallIgnite == null || 0l.equals(lastCallIgnite.time)) {
            val result = lastCallTableDef.get(get)
            val lastCallHbase = result.getValue(subscriberLastCallTableCF, subscriberLastCallTimeColumn)
            if (lastCallHbase != null ) {
              cache.putIfAbsent(cdr.MSISDN, LastCallBack(cdr.OPENING_TIME.getTime, CDR.isWorkingTime(cdr.OPENING_TIME)))
              if ( Bytes.toLong(lastCallHbase) < cdr.OPENING_TIME.getTime) {
                val subscriberLastCall = new Put(Bytes.toBytes(cdr.MSISDN))
                subscriberLastCall.addColumn(subscriberLastCallTableCF, subscriberLastCallTimeColumn, Bytes.toBytes(cdr.OPENING_TIME.getTime))
                subscriberLastCall.addColumn(subscriberLastCallTableCF, subscriberLastCallIsWorkingTimeColumn, Bytes.toBytes(CDR.isWorkingTime(cdr.OPENING_TIME)))
                subscriberLastCalls.add(subscriberLastCall)

                // check if
                val days = TimeUnit.MILLISECONDS.toDays( Calendar.getInstance().getTime.getTime - cdr.OPENING_TIME.getTime)
                if (days > 30) {
                  events.add(Event(cdr.MSISDN, "More than 30 days", cdr.OPENING_TIME.getTime))
                }
              }
            } else {
              cache.putIfAbsent(cdr.MSISDN, LastCallBack(cdr.OPENING_TIME.getTime, CDR.isWorkingTime(cdr.OPENING_TIME)))
              val subscriberLastCall = new Put(Bytes.toBytes(cdr.MSISDN))
              subscriberLastCall.addColumn(subscriberLastCallTableCF, subscriberLastCallTimeColumn, Bytes.toBytes(cdr.OPENING_TIME.getTime))
              subscriberLastCall.addColumn(subscriberLastCallTableCF, subscriberLastCallIsWorkingTimeColumn, Bytes.toBytes(CDR.isWorkingTime(cdr.OPENING_TIME)))
              subscriberLastCalls.add(subscriberLastCall)
            }
          } else {
            if (lastCallIgnite.time < cdr.OPENING_TIME.getTime) {
              cache.put(cdr.MSISDN, LastCallBack(cdr.OPENING_TIME.getTime, CDR.isWorkingTime(cdr.OPENING_TIME)))
            }
          }

          lastCallTableDef.put(subscriberLastCalls)
          lastCallTableDef.close()
          events
        }).toDF().write.mode(SaveMode.Append).parquet(eventParquetDir)

        // put to lac
        rdd.flatMap(cdr => {
            val notifications = new util.ArrayList[Notification]()

            val topic = "InLac"+"."+cdr.LAC
            val topicSubscribers  : List[Map[String, String]] = if (subsVals.contains(topic)) subsVals(topic)  else  Nil
            for ( subscription <- topicSubscribers )
                notifications.add(notificationFromMap(cdr.MSISDN, cdr.OPENING_TIME, subscription))
             notifications
             //topic
          }).keyBy(n => (n.subscriber , n.lac, n.subscriberName, n.notificationType, n.notificationText  )).reduceByKey((n1, n2) => {
          if (n1.timeLacReach < n2.timeLacReach)
            n1
          else n2
        }).map(row => row._2).toDF().write.mode(SaveMode.Append).parquet(notificationParquetDir)
      }

      if (!rdd.partitions.isEmpty) {
        fileRegExp.findAllMatchIn(rdd.toDebugString).foreach(name => {
          println(name.toString())
          if (ActionType.remove.equals(fileActionCommand)) {
            logger.info("Deleting processed File(s): " + name.toString)
            deleteFile(sc, name.toString)
          } else if (ActionType.move.equals(fileActionCommand)) {
            logger.info("Moving processed File(s): " + name.toString)
            moveFile(sc, name.toString(), filesCopyDir)
          } else if (ActionType.copy.equals(fileActionCommand)) {
            logger.info("Moving processed File(s): " + name.toString)
            copyFile(sc, name.toString(), filesCopyDir)
          }
        })
      }

    })

    ssc.start()
    ssc.awaitTermination()
  }

  def createSubscriberTable(): Unit = {
    val tableDescriptor = new HTableDescriptor(TableName.valueOf(subscriberTable))
    val cf = new HColumnDescriptor(subscriberTableCF)
    cf.setMaxVersions(hbaseMaxVersions)
    tableDescriptor.addFamily(cf)

    admin.createTable(tableDescriptor)
  }

  def createSubscriberLastCallTable(): Unit = {
    val tableDescriptor = new HTableDescriptor(TableName.valueOf(subscriberLastCallTable))
    val cf = new HColumnDescriptor(subscriberLastCallTableCF)
    cf.setMaxVersions(hbaseMaxVersions)
    tableDescriptor.addFamily(cf)

    admin.createTable(tableDescriptor)
  }

  def createClientPropsTable(): Unit = {
    val tableDescriptor = new HTableDescriptor(TableName.valueOf(clientPropsTable))
    val cf = new HColumnDescriptor(clientPropsTableCF)
    tableDescriptor.addFamily(cf)

    admin.createTable(tableDescriptor)
  }

  def notificationFromMap(subscriberPhone: String, timeLacReach: Timestamp,
                          map: util.Map[String, String]): Notification = {
    new Notification(
      subscriberPhone,
      map.get(Bytes.toString(subscriberLacColumn)),
      timeLacReach.getTime,
      System.currentTimeMillis(),
      map.get(Bytes.toString(subscriberNameColumn)),
      map.get(Bytes.toString(subscriberNotificationTypeColumn)),
      map.get(Bytes.toString(subscriberNotificationTextColumn))
    )
  }

  def deleteFile(sc: SparkContext, fileName: String): Unit = {
    val filePath = new Path(fileName)
    val fs = FileSystem.get(new Configuration())
    if (fs.isDirectory(filePath)) {
      fs.listStatus(filePath).foreach((status) => {
        fs.delete(status.getPath(), true)
      })
    } else {
      fs.delete(filePath, true)
    }
  }

  def moveFile(sc: SparkContext, fileName: String, filesCopyDir: Path): Unit = {
    val filePath = new Path(fileName)
    val fs = FileSystem.get(new Configuration())
    if (!fs.isDirectory(filePath)) {
      fs.copyFromLocalFile(true, filePath, filesCopyDir)
    }
  }

  def copyFile(sc: SparkContext, fileName: String, filesCopyDir: Path): Unit = {
    val filePath = new Path(fileName)
    val fs = FileSystem.get(new Configuration())
    if (!fs.isDirectory(filePath)) {
      fs.copyFromLocalFile(false, filePath, filesCopyDir)
    }
  }
}