package priospark.service

import java.util.concurrent.CopyOnWriteArrayList

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.hbase.{HColumnDescriptor, HTableDescriptor, TableName, HBaseConfiguration}
import org.apache.hadoop.hbase.client.{Put, ConnectionFactory}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import priospark.api.ClientProp

import scala.util.Try

/**
 * Created by vitalyshpak on 5/3/16.
 */
object SubscriberProps {

  final val clientPropsTable = Bytes.toBytes("CLIENT_PROPS")

  final val clientPropsTableCF = Bytes.toBytes("client_props_cf")


  final val clientPropsPhoneTypeColumn = Bytes.toBytes("phone_type")
  final val clientPropsIsBlacklistedColumn = Bytes.toBytes("is_blacklisted")
  final val clientPropsIs3g = Bytes.toBytes("is_3g")

  val conf = HBaseConfiguration.create()
  val connection = Try(ConnectionFactory.createConnection(conf)).orElse(throw new RuntimeException()).get
  val admin = connection.getAdmin()


  val config = ConfigFactory.load()
  val master = config.getString("masterUrl")
  val interval = config.getInt("interval")
  val sourceDir = config.getString("sourseDir")
  val propsDir = config.getString("propsDir")


  def main(args: Array[String]) {
    val sc = new SparkContext(master, "SubscriberProps", new SparkConf().set("spark.scheduler.mode", "FAIR"))
    val ssc = new StreamingContext(sc, batchDuration = Seconds(interval))
    val sqlContext = new SQLContext(sc)

    if (!admin.isTableAvailable(TableName.valueOf(clientPropsTable)))
      createClientPropsTable()


    val df = sqlContext.read.json(propsDir)


    df.map(prps => {

      val phone = prps.getString(prps.fieldIndex("msisdn"))
      val phoneType = if ( prps.getString(prps.fieldIndex("phone_type")) == "smartphone") 1 else 0
      val isBlacklisted = prps.getBoolean(prps.fieldIndex("is_blacklisted"))
      val is_3g = prps.getBoolean(prps.fieldIndex("is_3g"))

      new ClientProp(phone, phoneType, isBlacklisted)


      /*
      val clientPropsTableDef = connection.getTable(TableName.valueOf(Bytes.toString(clientPropsTable)))

      val clientProps = new CopyOnWriteArrayList[Put]()

      val put = new Put(Bytes.toBytes(phone))
      put.addColumn(clientPropsTableCF, clientPropsPhoneTypeColumn, Bytes.toBytes(phoneType))
      put.addColumn(clientPropsTableCF, clientPropsIsBlacklistedColumn, Bytes.toBytes(isBlacklisted))
      clientProps.add(put)

      clientPropsTableDef.put(clientProps)
      clientPropsTableDef.close()
      */


    }).foreachPartition(iter => {
      val clientPropsTableDef = connection.getTable(TableName.valueOf(Bytes.toString(clientPropsTable)))
      val clientProps = new CopyOnWriteArrayList[Put]()
      iter.foreach(data => {
        val put = new Put(Bytes.toBytes(data.phone))
        put.addColumn(clientPropsTableCF, clientPropsPhoneTypeColumn, Bytes.toBytes(data.phoneType))
        put.addColumn(clientPropsTableCF, clientPropsIsBlacklistedColumn, Bytes.toBytes(data.isBlacklisted))
        clientProps.add(put)
      })
      clientPropsTableDef.put(clientProps)
      clientPropsTableDef.close()
    })

    //df.show()

  }


  def createClientPropsTable(): Unit = {
    val tableDescriptor = new HTableDescriptor(TableName.valueOf(clientPropsTable))
    val cf = new HColumnDescriptor(clientPropsTableCF)
    tableDescriptor.addFamily(cf)

    admin.createTable(tableDescriptor)
  }


}



