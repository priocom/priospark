package priospark.service

import com.typesafe.config.ConfigFactory
import priospark.util.Json

/**
 * Created by vitalyshpak on 5/2/16.
 */
class CDRSubscriptionProcess () {


  val subscrVals: Map[String,List[Map[String,String]]] = initSubscr()

  def initSubscr() : Map[String,List[Map[String,String]]] = {

  val config = ConfigFactory.load()
  val subscriptionFile = config.getString("subscriptionFile")

    val source = scala.io.Source.fromFile(subscriptionFile)
    val lines = try source.mkString finally source.close()
    //val map : Map[String,List[Map[String,String]]] = Json.toMap[List[Map[String,String]]](lines)

    val map: Map[String,List[Map[String,String]]] = Json.fromJsonToMap(lines)
    //println ( map.keys)
    return  map

  }

  // get topic values
  def getTopicSubscr(topic : String) : List[Map[String, String]] = {
    if (subscrVals.contains(topic)) subscrVals(topic)  else  Nil
  }

  def getSubscriberSize() : Int = {
    subscrVals.keys.size
  }


}
