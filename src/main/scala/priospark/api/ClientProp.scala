package priospark.api

/**
 * Created by jcrm on 20.04.16.
 */
case class ClientProp(phone: String, phoneType: Int, isBlacklisted: Boolean) extends Serializable