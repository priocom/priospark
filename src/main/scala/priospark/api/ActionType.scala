package priospark.api

/**
 * Created by jcrm on 01.06.16.
 */
object ActionType extends Enumeration{
  type ActionClass = Value
  val remove, move, copy = Value
}
