package priospark.api

/**
 * Created by jcrm on 15.04.16.
 */
case class Notification(subscriber: String, lac: String, timeLacReach: Long,
                   recordCreationTime: Long, subscriberName: String,
                    notificationType: String, notificationText: String) extends Serializable {



}