package priospark.api

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.Calendar

/**
 * Created by jcrm on 18.04.16.
 */
case class CDR(CHARGING_ID: String,
               NODEID: String,
               SEQUENCE_NUMBER: String,
               MSISDN: String,
               IMSI: String,
               OPENING_TIME: Timestamp,
               SERVED_IP_ADDRESS: String,
               SGSNADDRESS: String,
               GGSNADDRESS: String,
               APN: String,
               DURATION: String,
               CHARGING_CHARACTERISTICS: String,
               DATA_VOLUME_UPLINK: String,
               DATA_VOLUME_DOWNLINK: String,
               CAUSE_FOR_RECCLOSING: String,
               ISROAMING: String,
               FILENAME: String,
               IMEISV: String,
               LAC: String,
               isWorkingTime: Boolean,
               optionalData: OptionalData
                ) extends Serializable

object CDR extends Serializable {

  def parseSensor(str: String): CDR = {
    val p = str.split(",")
    new CDR(
      p(0),
      p(1),
      p(2),
      p(3),
      p(4),
      new Timestamp(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse(p(5).trim).getTime),
      p(6),
      p(7),
      p(8),
      p(9),
      p(10),
      p(11),
      p(12),
      p(13),
      p(14),
      p(15),
      p(16),
      p(17),
      if (p.size > 25 && !p(25).isEmpty) p(25) else "",
      isWorkingTime(new Timestamp(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse(p(5).trim).getTime)),
      new OptionalData(p(18),
      p(19),
      p(20),
      p(21),
      p(22),
      p(23),
      p(24),
      if (p.size > 26) p(26) else "",
      if (p.size > 27) new Timestamp(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse(p(27).trim).getTime)
      else new Timestamp(System.currentTimeMillis()))
    )
  }

  def isWorkingTime(time: Timestamp): Boolean = {
    val localDateTime = time.toLocalDateTime()
    val hour = localDateTime.getHour
    val dayOfWeek = localDateTime.getDayOfWeek.getValue
    !(hour <= 8 || hour >= 18 || dayOfWeek > 5)
  }
}

case class OptionalData(NUMBER_OF_PART: String,
                   RATING_GROUP: String,
                   DATA_VOLUME_RG_UPLINK: String,
                   DATA_VOLUME_RG_DOWNLINK: String,
                   RESULT_CODE: String,
                   RATTYPE: String,
                   GSNPLMNIDENTIFIER: String,
                   CELL: String,
                   TIMEOFFIRSTUSAGE: Timestamp) extends Serializable{}