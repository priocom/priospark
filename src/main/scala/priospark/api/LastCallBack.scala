package priospark.api

/**
 * Created by jcrm on 30.05.16.
 */
case class LastCallBack (time: Long, isWorkingTime : Boolean) extends Serializable
