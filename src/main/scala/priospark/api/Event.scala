package priospark.api

/**
 * Created by vitalyshpak on 5/3/16.
 */
case class Event(subscriber: String, eventType: String, eventDate:Long) extends Serializable {

}
