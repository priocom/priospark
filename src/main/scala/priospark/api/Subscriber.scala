package priospark.api

import java.sql.{Timestamp}

/**
 * Created by jcrm on 15.04.16.
 */
case class Subscriber(name: String, lac: String,
                      timeFrom: Timestamp, timeTo: Timestamp,
                      notificationType: String,
                      notificationAddress: String,
                      notificationText: String) extends Serializable