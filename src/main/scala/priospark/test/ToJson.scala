package priospark.test

/**
 * Created by vitalyshpak on 5/2/16.
 */

import priospark.util.Json
import priospark.service.CDRSubscriptionProcess

object ToJson {

  def main(args: Array[String]): Unit = {
    val json: String = "{\"a\":[1,2],\"b\":[3,4,5],\"c\":[]}"
    //val map : Map[String,Seq[Int]] = Json.toMap[Seq[Int]](json)

    val subs = new CDRSubscriptionProcess

    val topic : String = "InLac"+"." + 1

    val v:List[Map[String, String]] = subs.getTopicSubscr(topic)


    for ( subscription <- v) println ("Subscription:" + subscription)

    //println (v)
  }
}
